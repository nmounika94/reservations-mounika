//
//  ReservationsTableViewCell.swift
//  Reservations
//
//  Created by Mounika Nerella on 9/10/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class ReservationsTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var partySizeCount: UILabel!
    @IBOutlet weak var partySizelabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var dateDisplayLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
