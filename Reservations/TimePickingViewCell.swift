//
//  TimePickingViewCell.swift
//  Reservations
//
//  Created by Mounika Nerella on 9/10/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class TimePickingViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeSelectView: UIImageView!
    @IBOutlet weak var timeDisplaylabel: UILabel!
}
