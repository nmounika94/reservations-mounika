//
//  SelectinginfoViewController.swift
//  Reservations
//
//  Created by Mounika Nerella on 9/10/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class Day {
    var strDay : String?
    var numDay : String?
    var isSelected: Bool?
    
    init(dayStr: String, dayNum : String, selected: Bool) {
        strDay = dayStr
        numDay = dayNum
        isSelected = selected
    }
}

class Time {
    var timeText : String?
    var isSelected: Bool?
    
    init(textTime : String,selected: Bool) {
        timeText = textTime
        isSelected = selected
    }
}



class SelectinginfoViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate {
    
    
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var priceDisplay: UILabel!

    @IBOutlet weak var monthDisplay: UILabel!
    
    @IBOutlet weak var timeDisplay: UICollectionView!
    @IBOutlet weak var dateDisplay: UICollectionView!
    
    @IBOutlet weak var partySizeBtn: UIButton!
    
    var currentMonth: String?
    var variableMonth: String?
    var dayText: String?
    var dayNumber: String?
    var time: String?
    var dayArray = [Day]()
    var timeArray = [Time]()
    var dateIsSelected = false
    var timeIsSelected = false
    var selectedDate: String?
    var selectedTime: String?
    var currentDate: String?
    var currentTime: String?
    let pickerViewArray = ["1","2","3","4","5","6","7","8","9","10","11","12"]
   
    override func viewWillAppear(_ animated: Bool) {
        title = "SCHEDULE"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "MMMM"
        currentMonth = dateFormatter.string(from: date)
        variableMonth = currentMonth
        getDatesArray()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let someDateTime = formatter.date(from: "2016/10/08 08:00")
        formatter.dateFormat = "EEE, MMMM dd, YYYY"
        currentDate = formatter.string(from: date)
       getTimeArray(someDate: someDateTime!)
        
        
        // Do any additional setup after loading the view.
    }

    func getDate(date: Date){
        
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        variableMonth = dateFormatter.string(from: date)
        print(variableMonth)
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        dayText = dateFormatter.string(from: date)
        print(dayText)
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        dayNumber = dateFormatter.string(from: date)
        print(dayNumber)
        dayArray.append(Day(dayStr: dayText!, dayNum: dayNumber!, selected: false))
    }
    func getTime(date: Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm a"//this gives the date in the format of hrs sec and AM or PM
        time = dateFormatter.string(from: date)
        print(time)
    }
    func getDatesArray(){
        var i = 0
        while variableMonth == currentMonth {
            let date = Calendar.current.date(byAdding: .day,value: i, to: Date())
            i += 1
            getDate(date: date!)
            print(date)
        }
    }
    func getTimeArray(someDate: Date) {
        let formatter = DateFormatter()
        timeArray = []
        var components = NSCalendar.current.dateComponents([.minute], from: someDate)
        let minute = components.minute ?? 0
        components.minute = minute >= 30 ? 60 - minute : -minute
        let someDate1 = Calendar.current.date(byAdding: components, to: someDate)

        
        for i in 1...15 {
            let modifiedDate = Calendar.current.date(byAdding: .hour,value: i, to: someDate1!)
            formatter.dateFormat = "hh:mm a"
            let time = formatter.string(from: modifiedDate!)
            if time == "09:00 PM" {
                break
            }
            timeArray.append(Time(textTime: time, selected: false))
        }
        print(timeArray)
    }
    
    
    
    @IBAction func partySizeCount(_ sender: UIButton) {
        
        UIView.animate(withDuration: 1) {
            
            
            //writeline
            
            
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func addInfo(_ sender: UIButton) {
        
        let pSize = partySizeBtn.titleLabel?.text
        let titleText = "Hot Stone Massage"
        let detail = " Massage focussed on the deepest layer of muscles to target knots and release chronic muscle tension."
        CoreManager.createNewObject(reservations: DesignModel(sizeParty: pSize, dates: selectedDate, details: detail, title1: titleText, availTime: selectedTime))
        navigationController?.popViewController(animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == dateDisplay{
           return dayArray.count
        }else{
            return timeArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == dateDisplay {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DatePickingCell", for: indexPath) as? DatePickingCell
            let reserveObj = dayArray[indexPath.row]
            cell?.dateLabel.text = reserveObj.numDay
            cell?.dayLabel.text = reserveObj.strDay
            if reserveObj.isSelected == true {
                cell?.checkBoximg.image = UIImage(named: "ok")
            }
            else{
                cell?.checkBoximg.image = nil
            }
            return cell!
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"TimePickingViewCell" , for: indexPath) as? TimePickingViewCell
            let timeObj = timeArray[indexPath.row]
            cell?.timeDisplaylabel.text = timeObj.timeText
            if timeObj.isSelected == true {
                cell?.timeSelectView.image = UIImage(named: "ok")
            }
            else{
                cell?.timeSelectView.image = nil
            }
         return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == dateDisplay {
            for item in dayArray {
                item.isSelected = false
            }
            for item in timeArray {
                item.isSelected = false
            }
            dateIsSelected = true
           timeIsSelected = false
            
            let cell = dayArray[indexPath.row]
            cell.isSelected = true
            
            dateDisplay.reloadData()
            timeDisplay.reloadData()
            
            if dateIsSelected == true && timeIsSelected == true {
                reserveBtn.isEnabled = true
                
            }
            else{
                reserveBtn.isEnabled = false
                reserveBtn.backgroundColor = UIColor.gray
            }
            
            selectedDate = "\(cell.strDay ?? "Tue"), \(currentMonth ?? "September") \(cell.numDay ?? "12"),2017"
            if currentDate == selectedDate {
                getTimeArray(someDate: Date())
            }
            else {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd HH:mm"
                let someDateTime = formatter.date(from: "2016/10/08 08:00")
                getTimeArray(someDate: someDateTime!)
            }
            
        }
        else {
            for item in timeArray {
                item.isSelected = false
            }
            timeIsSelected = true
            
            let cell = timeArray[indexPath.row]
            cell.isSelected = true
            timeDisplay.reloadData()
            
            if dateIsSelected == true && timeIsSelected == true {
                reserveBtn.isEnabled = true
                
            }
            else {
                reserveBtn.isEnabled = false
                reserveBtn.backgroundColor = UIColor.gray
            }
            
            selectedTime = cell.timeText
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        partySizeBtn.setTitle(pickerViewArray[row], for: .normal)
        priceDisplay.text = "$\(Double(row+1) * 120.00)0"
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewArray.count
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
