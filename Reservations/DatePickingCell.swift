//
//  DatePickingCell.swift
//  Reservations
//
//  Created by Mounika Nerella on 9/10/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class DatePickingCell: UICollectionViewCell {
    
    @IBOutlet weak var checkBoximg: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
   }
