//
//  ReservationDisplayController.swift
//  Reservations
//
//  Created by Mounika Nerella on 9/10/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import  CoreData
class ReservationDisplayController: UITableViewController,NSFetchedResultsControllerDelegate {
    
    var context: NSManagedObjectContext! {
        return CoreManager.persistentContainer.viewContext
    }
    var fetchResults : NSFetchedResultsController<ReservationInfo>?

    override func viewDidLoad() {
        super.viewDidLoad()
        let fetchRequest = NSFetchRequest<ReservationInfo>(entityName: "ReservationInfo")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeAvail",ascending: false),NSSortDescriptor (key:"numDay",ascending: true)]
        fetchResults = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchResults?.delegate = self
        do{
            try fetchResults?.performFetch()
        }
        catch{
            
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        tableView.reloadData()
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if let frc = fetchResults {
            return frc.sections!.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.fetchResults?.sections else {
            fatalError("No sections in fetchedResults")
        }
       let sectionsInfo = sections[section]
        return sectionsInfo.numberOfObjects
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationsTableViewCell", for: indexPath) as? ReservationsTableViewCell
        guard let reservation = self.fetchResults?.object(at: indexPath)
            else {
            fatalError("No Value at the cell")
        }
      cell?.dateDisplayLabel.text = reservation.numDay
        cell?.descriptLabel.text = reservation.descript
        cell?.partySizeCount.text = reservation.partySize
        cell?.partySizelabel.text = reservation.partySize
        cell?.timelabel.text = reservation.timeAvail
        cell?.titleLabel.text = reservation.title
        
        return cell!
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
