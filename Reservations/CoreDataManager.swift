//
//  CoreDataManager.swift
//  Reservations
//
//  Created by Mounika Nerella on 9/10/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
import CoreData
import UIKit

struct DesignModel {
    var sizeParty : String?
    var dates : String?
    var details : String?
    var title1: String?
    var availTime: String?
}

class CoreManager {
    static func createNewObject(reservations: DesignModel) {
        
        let designobject = NSEntityDescription.insertNewObject(forEntityName: "ReservationInfo", into:persistentContainer.viewContext ) as! ReservationInfo
        designobject.numDay = reservations.dates
        designobject.partySize = reservations.sizeParty
        designobject.descript = reservations.details
        designobject.timeAvail = reservations.availTime
        designobject.title = reservations.title1
    }
    static func read() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ReservationInfo")
        let sortDescriptors = NSSortDescriptor(key: "title", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptors]
        do{
            guard let reserve = try persistentContainer.viewContext.fetch(fetchRequest) as? [ReservationInfo] else {
                return
            }
            for i in reserve {
                print(i.numDay ?? "No days available",i.partySize ?? "no vacancy",i.descript ?? "no info",i.timeAvail ?? "no availability",i.title ?? "no title")
            }
        }
        catch{}
    }
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Reservations")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error),\(error.userInfo)")
            }
        })
       return container
    }()
    static func saveContext () {
        let context = persistentContainer.viewContext
        
        if context.hasChanges{
            do {
                try context.save()
            }catch {
                 let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
